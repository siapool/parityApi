package com.mchain.pojo;

public class TransactionResp {
	String txHash;
	Error error;
	
	public TransactionResp(Error e) {
		this.txHash = null;
		this.error = e;
	}
	
	public TransactionResp(String tx) {
		this.txHash = tx;
		this.error = null;
	}
	
	public String getTxHash() {
		return txHash;
	}
	public Error getError() {
		return error;
	}
	
	
}
