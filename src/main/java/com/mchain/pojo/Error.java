package com.mchain.pojo;

public class Error {
	
	public static final int JAVA_EXCEPTION_ID = -1;
	public static final String JAVA_EXCEPTION_MSG = "java.exception";
	
	public static final int BAD_REQUEST_ID = -9;
	public static final String BAD_REQUEST_MSG = "bad.request";
	
	public static final int COMMON_ERROR_ID = 0;
	public static final String COMMON_ERROR_MSG = "common.error";
	
    private int code;
    private String message;
    private String data;
    
    public Error() {
    }

    public Error(int code, String message) {
        this.code = code;
        this.message = message;
    }
    
    public Error(org.web3j.protocol.core.Response.Error e) {
    	this.code = e.getCode();
    	this.message = e.getMessage();
    	this.data = e.getData();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
