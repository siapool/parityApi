package com.mchain.pojo;

/**
 * Created by leo on 2017/7/12.
 */
public class Account {
	
    private String accoundId;
    private String password;
    private String accountName;
    
    public String getAccoundId() {
        return accoundId;
    }

    public void setAccoundId(String accoundId) {
        this.accoundId = accoundId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
}
