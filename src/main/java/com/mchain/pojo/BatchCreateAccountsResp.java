package com.mchain.pojo;

import java.util.ArrayList;
import java.util.List;

public class BatchCreateAccountsResp {
	int total;
	int successCount;
	int failedCount;
	
	List<Account> accounts;
	Error lastError;
	
	public BatchCreateAccountsResp(int total) {
		this.total = total;
		this.accounts = new ArrayList<Account>();
	}

	public Error getLastError() {
		return lastError;
	}

	public void setLastError(Error lastError) {
		this.lastError = lastError;
	}
	
	public void success() {
		this.successCount ++;
	}
	
	public void fail() {
		this.failedCount ++;
	}
	
	public void addAccount(Account acc) {
		this.accounts.add(acc);
		this.successCount ++;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}

	public int getFailedCount() {
		return failedCount;
	}

	public void setFailedCount(int failedCount) {
		this.failedCount = failedCount;
	}
	
}
