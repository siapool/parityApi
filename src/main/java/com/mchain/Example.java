package com.mchain;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.web3j.utils.Convert;

import com.mchain.api.ParityWrapper;
import com.mchain.pojo.BatchCreateAccountsResp;
import com.mchain.pojo.TransactionResp;

public class Example {

	public static void main(String[] args) {
		//注意：
		//    1. 启动parity时候请开启personal,parity_accounts,eth模块，例如--jsonrpc-apis web3,eth,net,personal,parity,parity_set,traces,rpc,parity_accounts
		//       parity启动参数参见：https://github.com/paritytech/parity/wiki/Configuring-Parity
		//    2. web3j通过jsonrpc与parity通信，但parity的接口有变化（personal模块变化最大），有些不方法已经不能用了，可以采用com.mchain.parity。MyParity的方式进行复写
		//		 parity jsonrpc接口，参见https://github.com/paritytech/parity/wiki/JSONRPC
		ParityWrapper parity = new ParityWrapper("http://127.0.0.1:8545");
		//批量创建账号
		//参数：int 所需创建账号的个数
		BatchCreateAccountsResp resp = parity.batchCreateAccount(10);
		System.out.println(resp.getSuccessCount());

		List<String> names = new ArrayList<String>();
		names.add("00a");
		names.add("00b");
		names.add("00c");
		//根据账号名称列表创建账号
		//参数： List<String> 账号名称列表
		resp = parity.batchCreateAccount(names);
		System.out.println(resp.getSuccessCount());

		//获取余额，返回余额， 单位eth
		//参数： String 账号地址
		//    boolean 是否必须等待同步完成， 若为true，则parity节点未同步完成时，返回null
		BigDecimal balance = parity.getBalance("0x.....", true);
		System.out.println(balance);
		//获取余额，按指定单位返回余额
		//参数： String 账号地址
		//	  org.web3j.utils。Convert.Unit 余额的单位
		//    boolean 是否必须等待同步完成， 若为true，则parity节点未同步完成时，返回null
		balance = parity.getBalance("0x.....", Convert.Unit.WEI, false);
		System.out.println(balance);

		BigInteger amount = Convert.toWei(new BigDecimal("0.0101"), Convert.Unit.ETHER).toBigInteger();
		BigInteger gasPrice = Convert.toWei("7546000001", Convert.Unit.WEI).toBigInteger();
		BigInteger gasLimit = Convert.toWei("21000", Convert.Unit.WEI).toBigInteger();
		BigInteger afterBlockNum = new BigInteger("4016070");
		//转账，返回转账的hash
		//参数：String 源账号地址
		//	  String 源账号密码
		//    String 目的账号地址
		//    BigInteger 转账金额
		//	  BigInteger gas price
		//	  BigInteger gas limit
		//	  String 转账data
		//	  BigInteger 延迟转账，指定转账区块
		//	  Long 延迟转账，指定转账时间（单位秒）
		//	                        若同时指定转账区块和转账时间，以区块为主
		//         若都不指定，为即时转账

		TransactionResp tx = parity.trans("0x.....", "*****", "0x.....", 
				amount, gasPrice, gasLimit, null, afterBlockNum, null);
		System.out.println(tx.getTxHash());

		Long time = System.currentTimeMillis() / 1000; //should use second
		tx = parity.trans("0x.....", "*****", "0x.....",
				amount, gasPrice, gasLimit, null, null, time);
		System.out.println(tx.getTxHash());
	}

}
