package com.mchain.parity;

import java.util.Arrays;

import org.web3j.protocol.Web3jService;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.parity.JsonRpc2_0Parity;

import com.mchain.parity.response.BooleanResponse;

/**
 * Created by leo on 2017/6/18.
 */
public class MyParity extends JsonRpc2_0Parity {
    public MyParity(Web3jService web3jService)
    {
       super(web3jService);
    }

    public Request<?, EthSendTransaction> personalSendTransaction(MyTransaction transaction, String pwd) {
        return new Request<>("personal_sendTransaction",
                Arrays.asList(new Object[]{transaction, pwd}),
                1L, this.web3jService,
                EthSendTransaction.class);
    }
    
    public Request<?, BooleanResponse> paritySetAccountName(String accountId, String newAccountName) {
        return new Request<>(
                "parity_setAccountName",
                Arrays.asList(accountId, newAccountName),
                ID,
                this.web3jService,
                BooleanResponse.class);
    }

}

